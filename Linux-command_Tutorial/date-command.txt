
Assignment of Date Command with its options

Display date in long format - date
Display date in the format of mm/dd/yy  - date +%D
Display time in HH:MM:SEC - date +%
Display time 24 hours format - date +%
Display only HH:MM - date +%
Display time in 12 hours format - date +%
Display date and time together by giving on tab space in between - date +%D%t%T
Display hours in 12 hours format - date +%
Display month number date +%
Display day number date +%
Display abbreviated weekday - date +%A
Display weekday in short - date +%
Display date on first line, time on second line and weekday on third line - date +%D%n%T%n%A
Display year number in 4 digit - date +%
Display only today’s date without month and year - date +%e
Display full abbreviated month name- date +%B
Display short form of abbreviated month name - date +%
Display day of year - date +%j
Display hours on first line, minutes on second line and seconds on third line and AM/PM on fourth line - date +
Display day of week number - date +%w
