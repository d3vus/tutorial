#!/bin/sh

# VARIABLE - PART - 2

# In shell many of the variable name are pre-assigned, they contain usefull info

# First ->>
# $0-$9 and $#

# $0 is for basename of the program as it is called.

# $1-$9 are first parameters passed to the script

# $@ is all parameters ( $1-$9 ).

# $* do the same job as $@ but it removes spaces and qoutes

# $# is number of parameters  the script was called with.

# Example

echo "There are $# parameters passed into this script"
echo "My name is $0"
echo "My first parameter is $1"
echo "My second parameter is $2"
echo "All of my parameters are $@"
echo "Again this is all of my parameters $*"

# We can take more than 9 parameters by using shift command

while [ "$#" -gt "0" ]
do
    echo "\$1 is $1"
    shift
done


# Other special variable is -->> #? which contains exit value of the last run command.

/usr/loacal/bin/my-command
if [ "$?" -ne "0" ] ; then
    echo "Sorry we had a problem there"
fi

# Other special variable are , $$ & $! both are process numbers. 

# -->> $$ => PID (process identifier) of current running shell.

# -->> $! => PID of last run background process. Useful to keep track of the process as it gets on with its job.

# Another is IFS. Internal field seperator. Default value is SPACE TAB NEWLINE, n=but if you are changing it , it is easier to take a copy , as shown:


old_ifs="$IFS"

IFS=:

echo "Please input some data seperated by colon(:) .... "
read x y z
IFS=$old_ifs
echo "x is $x y is $y z is $z"



# Game of brackets

