# FROM THIS [GUIDE](https://www.shellscript.sh/philosophy.html)

 
 █████╗ ██████╗  ██████╗ ██╗   ██╗████████╗    ███████╗██╗  ██╗███████╗██╗     ██╗
██╔══██╗██╔══██╗██╔═══██╗██║   ██║╚══██╔══╝    ██╔════╝██║  ██║██╔════╝██║     ██║
███████║██████╔╝██║   ██║██║   ██║   ██║       ███████╗███████║█████╗  ██║     ██║
██╔══██║██╔══██╗██║   ██║██║   ██║   ██║       ╚════██║██╔══██║██╔══╝  ██║     ██║  I
██║  ██║██████╔╝╚██████╔╝╚██████╔╝   ██║       ███████║██║  ██║███████╗███████╗███████╗
╚═╝  ╚═╝╚═════╝  ╚═════╝  ╚═════╝    ╚═╝       ╚══════╝╚═╝  ╚═╝╚══════╝╚══════╝╚══════╝
                                                                                      


## History of Shell Scripting - 


1. sh also known as shell or Bourne shell originally was created by steve bourne in 7th edition of Bell labs research version of unix. 
2. There are many types of shell currently present - sh, ksh, zsh, csh, fish etc.


## Philosphy of shell scripting -

Shell script is kind of not popular among many devs because -

1. Due to its being interpreter language (means relatively slow language as compared to others even slower than perl interpreted language).
2. Due to its being too easy to write , so there is a lot of poor quality shell-scripts.


## How to write good,clean and quick shell scripts -

1. Most important is to write clear , readable layout.
2. Avoid unnecessary comments.


### Note:-

```
Clear layout can make a huge differnce by making it understandable and maintanable , maybe one day a simple script can become complex one and if nobody else understood it then you will be only one to maintain it.
```

## Important

Since it is scripting language , indentation is very useful.

Also some of the lines / commands which are also very effective - 

` cat /tmp/myfile | grep "filename" `

and

` grep "filename" /tmp/myfile `




