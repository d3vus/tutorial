#! /bin/sh

# test also described as '['(it is symlink to /usr/bin/test) in unix/linux
# test is simple but powerful 

# if [$foo = "bar" ] will not work as it will be interpreted as if test$foo = "bar" ] -- which is ] without [ .
# if [ $foo = "bar" ]

# Also in shell programming we use "=" instead of "==" for eqating for string and -eq for integer.

# Generally test is used with if and width

# Syntax of IF-Else

# if [ ... ]
# then
#     statement
# else
#     statement
# fi

# OR

# if and then can be on the same line
# if [ ... ] ; then
#     statement
# fi

# Syntax of if-elif-else

# if [ Something ]
# then
#     statement
# elif
# then
#     statement
# else
#     statement
# fi


if [ "$x" -lt "0" ]
then
    echo "$x is less than 0"
fi

if [ "$x" -gt "0" ] ; then
    echo "$x is grater than 0"
fi

[ "$x" -le "0" ] && \
    echo "$x is less than equal to 0" \ ||
	 echo "$x is greater/more than 0 \"hoho\""
	 

[ "$x" -ge "0" ] && \
    echo "$x is greater than equal to 0"

[ "$x" = "hello" ] && \
    echo "$x is string \"hello\""

[ "$x" != "hello" ] && \
    echo "$x is not string \"hello\""

[ -n "$x" ] && \
    echo "$x is of nonzero length"

[ -f "$x" ] && \
    echo "$x is the path of a real file" || \
	echo "No such file: $x"

[ -x "$x" ] && \
    echo " $x is the path of an executable file"

[ "$x" -nt "/etc/passwd" ] && \
    echo "$x is a file which is newer than /etc/passwd"

[ $x -ne "0" ] && echo " $x isn't zero" || echo "$x is zero"

[ -f $x ] && echo "$x is a file" || echo "$x is not a file"

[ -n $x ] && echo "$x is of non-zero length" || echo "x is of zero length"

# Script to check the content of the varibale

echo -en "Please guess the magic number: "

read x

echo $x | grep "[^0-9]" > /dev/null 2>&1

if [ "$?" -eq "0" ]; then
    # if the grep found something other than 0-9
    # then it's not an integer
    echo "Sorry, wanted an number"
else
    # The grep found only 0-9, so it's an integer
    # We can safely do a test on it.
    if [ $x -eq "7" ] ; then
	echo "You entered the magic number"
    fi
fi

y=0 
while [ -n "$y" ]
do
    echo "Enter some text (RETUEN to quit)"
    read y
    echo "You said: $y"
done

# A other more tidy script

z=0
while [ -n "$z" ]
do
    echo "Enter some text (RETURN to quit)"
    read z
    if [ -n "$z" ] ; then
	echo "You said: $z"
    fi
done
