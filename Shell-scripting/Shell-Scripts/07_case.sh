#!/bin/sh

# CASE -->> in place of if..then..else

# Syntax

echo "Please talk to me ..."

while :
do
    read input_string
    case $input_string in
	hello)
	    echo "Hello Yourself!"
	    ;;
	bye)
	    echo "See you again!"
	    break
	    ;;
	*)
	    echo "Sorry, i don't understand"
	    ;;
    esac
done
echo
echo "That's all folks"


	      
