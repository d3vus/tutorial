#!/bin/sh 

# Written at top to inform /bin/sh in most unix system (/bin/sh is symlink to /bin/bash in many linux system) to execute this script.
# This is a comment
# is different than #!. 

echo "Hello      World" # This is a comment too -- Here HEllo World passed as a one arg.
echo Hello     World  # Here Hello World passed as 2 different arg. Hello and World

echo "Hello * world"
echo Hello * World
echo "Hello" World
echo Hello "    " World
echo "Hello  "*"  world"
echo `hello` world
echo 'hello' world
