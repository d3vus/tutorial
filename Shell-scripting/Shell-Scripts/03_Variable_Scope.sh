#! /bin/sh


x="hello"
expr $x + 1 # Try it and enjoy it.
expr 2 + 2 # Try to maintain space between digits and operator

# Varibales which are not defined , echo them will not produce an error just they will output an empty space.

echo "MYVAR is : $MYVAR" # Earlier MYVAR is not defined yet
# Scope of MYVAR is started from below line.
MYVAR="hi there"
echo "MYVAR is : $MYVAR"

echo "What is your name?"
read USER_NAME
echo "Hello $USER_NAME"
echo "I will create you a file called ${USER_NAME}_File"

# Importance of double qoute here and why it is needed in case you are having std input like 2 words name 
# Check its result by ls
touch ${USER_NAME}_file
# Check its result by ls
touch "${USER_NAME}_file"
