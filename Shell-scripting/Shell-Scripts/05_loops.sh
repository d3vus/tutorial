#! /bin/sh


# Syntax
# For -- Loopecho

# for i in  range_of_values
# do
#     echo "$i"
# done


for i in 1 2 3 4 5
do
    echo "Looping..... number $i"
done

for i in hello 1 \* 2 goodbye
do 
    echo "Looping .... i is set to $i"
done

for i in hello 1 2 goodbye
do 
    echo "Looping .... i is set to $i"
done

##################################

# While -- loops
# Syntax

# while [ condition ]
# do
#     statements ...
#     .......
# done

INPUT_STRING="hello"

while [ "$INPUT_STRING" != "bye" ]
do
    echo "Give me some input (or bye to quit)"
    read INPUT_STRING
    echo "You have typed - ${INPUT_STRING}"
done



# Colon (:) always evaulate to true , now in below you need a true method to quit the loop
while :
do
    echo "Give me some input (or ^c to quit)"
    read INPUT_STRING
    echo "You typed $INPUT_STRING"
done


# While read loop

while read input_text
do
    case $input_text in
	hello)    echo English      ;;
	howdy)    echo American     ;;
	gday)     echo Australian   ;;
	bonjour)  echo French       ;;
	"guten tag")    echo German ;;
	*)        echo Unknown Language : $input_text ;;
    esac
done < myfile.txt
