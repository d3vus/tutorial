#! /bin/sh

# There should be no space around "=" during declaration. 

# How to create variable in shell
MY_MESSAGE="Hello World"
# try this also - MY_MESSAGE="Hello" World
MY_PIE=3.142
MY_ANOTHER_PIE="3.142"
MY_NUMBER=1
MY_MIXED=123abc

# How to call them
echo $MY_MESSAGE
echo $MY_MIXED
echo "MY_PIE"

hello="hi"

# Take input from command line using - read , so, read works as a input in other programming languages.
echo what is your name?
read MY_NAME # Like taking input in other programming
echo "`hello` $MY_NAME -  hope you're well."


